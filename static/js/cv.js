(function(window, document) {
    'use strict';
    var bcback = document.getElementsByClassName('business-card-back')[0];
    var bcborder = document.getElementsByClassName('business-card-border')[0];
    var bclinks = document.getElementsByClassName('link-on-bc');
    bcborder.addEventListener('mouseenter', onCard('normal'));
    bcborder.addEventListener('mouseleave', onCard('reverse'));
    for(var i = 0; i < bclinks.length; i++){
        bclinks[i].addEventListener('mouseenter', onLink('normal'));
        bclinks[i].addEventListener('mouseleave', onLink('reverse'));
    }
    bcborder.onclick = clickCard;
    bcback.onclick = clickCard;
    var drawBorder =
        {
          targets: '.business-card .business-card-border',
          strokeDashoffset: [anime.setDashoffset, 0],
          easing: 'easeInOutSine',
          duration: 300,
        };
    var dropShadowR = {
          targets: '.business-card .bcshadow',
          points: [
              { value:
                  ['372,1571 372,1603 1021,1603 1021,528 987,528 987,1571 372,1571',
                  '372,1571 372,1571 987,1571 987,528 987,528 987,1571 372,1571']
              }
          ],
          easing: 'easeOutQuad',
          duration: 300
        };
    var dropShadow = {
          targets: '.business-card .bcshadow',
          points: [
              { value:
                  ['372,1571 372,1571 987,1571 987,528 987,528 987,1571 372,1571',
                  '372,1571 372,1603 1021,1603 1021,528 987,528 987,1571 372,1571']
              }
          ],
          easing: 'easeOutQuad',
          duration: 300
        };
    function onCard(drct){
        return function (){
            if(!bcborder.classList.contains('disableHover') || drct == 'reverse'){
                if(drct == 'reverse'){
                    bcborder.classList.remove('disableHover');
                }
                var tl = anime.timeline({
                  easing: 'easeOutExpo',
                  duration: 0,
                  direction: drct
                });
                tl
                .add(drawBorder);
            }
        }
    };
    function onLink(drct){
        return function (){
            if(drct == 'reverse'){
                if(!bcborder.classList.contains('disableHover')){
                    bcborder.classList.remove('disableHover');
                }
                anime({
                  targets: '.business-card .business-card-border-opposite',
                  strokeDashoffset: [0,anime.setDashoffset],
                  easing: 'easeInOutSine',
                  duration: 300,
                });
            }else{
                anime({
                  targets: '.business-card .business-card-border-opposite',
                  strokeDashoffset: [anime.setDashoffset, 0],
                  easing: 'easeInOutSine',
                  duration: 300,
                });
            }
        }
    };
    function clickCard(){
        var bcbOp = window.getComputedStyle(bcback).getPropertyValue("opacity");

        if(bcbOp > 0){
            var tl = anime.timeline({
              easing: 'easeOutExpo',
              duration: 0,
            });
            tl
            .add(drawBorder)
            .add(dropShadow, '-=300')
            .add({
                targets: '.business-card .business-card-back',
                opacity: [bcbOp,0],
                easing: 'easeInOutSine',
                duration: 500,
                complete: function(){
                        bcborder.classList.add('disableHover');
                        bcback.style.display = "none";}
            })
            .add(dropShadowR);
        }else{
            var tl = anime.timeline({
              easing: 'easeOutExpo',
              duration: 0,
            })
            .add(dropShadow);
            tl
            .add({
                targets: '.business-card .business-card-back',
                opacity: [bcbOp, 1],
                easing: 'easeInOutSine',
                duration: 500,
                begin: function(){
                        bcback.style.display = "inline";}
            })
            .add(dropShadowR);
        }
    };
    function loadCard(){
        var bcloading = document.getElementsByClassName('loading')[0];
        var content = document.getElementsByClassName('container')[0];
        var tl = anime.timeline({
          easing: 'easeOutExpo',
          duration: 0,
          direction: 'reverse'
        });
        tl
        .add({
          targets: '.business-card .bcbox',
          points: [
              { value:
                  ['338,1538 338,1572 371,1572 371,1538',
                  '371,1538 371,1538 371,1538 371,1538']
              }
          ],
          easing: 'easeOutQuad',
          duration: 260
        })
        .add({
          targets: '.business-card .line',
          strokeDashoffset: [0,anime.setDashoffset],
          delay: function(el, i) { return i * 75 },
          easing: 'easeInOutSine',
          duration: 200
        })
        .add(drawBorder)
        .add(dropShadow)
        .add({
          targets: '.loading',
          opacity: [0,1],
          easing: 'easeInOutSine',
          duration: 300,
          complete: function(){bcloading.style.display = "none";
                                    return 0;}
        })
        .add({
          targets: '.loading .loaded',
          opacity: [1,0],
          easing: 'easeInOutSine',
          duration: 300
        }, '+=500')
        .add({
          targets: '.loading .override',
          strokeDashoffset: [0,anime.setDashoffset],
          easing: 'easeInOutQuad',
          duration: 500
        })
        .add({
          targets: '.loading .progress',
          opacity: [0,1],
          easing: 'easeInOutQuad',
          duration: 200
        })
        .add({
          targets: '.loading .progress',
          width: [1340.9,0],
          easing: 'spring(1, 55, 15, 0)',
          duration: 500
        })
        .add({
          targets: '.loading .logo',
          opacity: [1,0],
          easing: 'easeInOutSine',
          duration: 300
        })
        .add({
          targets: '.container',
          opacity: [1,0],
          easing: 'easeInOutSine',
          duration: 30
        })
        .add({
          targets: '.business-card .business-card-border-opposite',
          strokeDashoffset: [anime.setDashoffset,0],
          easing: 'easeInOutSine',
          duration: 30,
        })
        .add({
          targets: '.loading .line',
          strokeDashoffset: [0,anime.setDashoffset],
          easing: 'easeInOutSine',
          duration: 500
        });
        setTimeout(function(){
                content.style.display = "block";
            }
            , 200);
    };
    loadCard();
    // japanese and english
    function displayShow (els) {
        els.forEach(function (el) {
            el.style.display='inline-block';
        });
    };
    function displayNone (els) {
        els.forEach(function (el) {
            el.style.display='none';
        });
    };
    function swapEnJp () {
        var en = [].slice.call(document.getElementsByClassName('en'));
        var jp = [].slice.call(document.getElementsByClassName('jp'));
        if (document.getElementById('lang-switch').value === 'EN/JP') {
            document.getElementById('lang-switch').value = 'JP/EN';
            displayNone(en);
            displayShow(jp);
        } else {
            document.getElementById('lang-switch').value = 'EN/JP';
            displayShow(en);
            displayNone(jp);
        }
    };
    document.getElementById('lang-switch').addEventListener('click', swapEnJp);
}(this, this.document));
