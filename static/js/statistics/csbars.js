function barchart() {

  var margin = {top: 300, right: 30, bottom: 10, left: 5 },
      width = 620, height = 60, mname = "mbar1";
  
  var MValue = "ACCESS";
  
  function barrender(selection) {
    selection.each(function(data) {
  
      var x = d3.scaleBand()
          .range([0, width]);
      
      var y = d3.scaleLinear()
          .rangeRound([height, 0]);
      
      var xAxis = d3.axisBottom(x)
          .tickFormat(d3.timeFormat(TFormat[TIntervals[TPeriod]]));
      
      var svg = d3.select(this).select("svg")
         .append("g")
            .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
            .attr("id", "access-volume");
      
      x.domain(data.map(function(d) { return d.TIMESTAMP; }));
      y.domain([0, d3.max(data, function(d) { return d[MValue]; })]).nice();
  
      var xtickdelta   = Math.ceil(60/(width/data.length))
      xAxis.tickValues(x.domain().filter(function(d, i) { return !((i+Math.floor(xtickdelta/2)) % xtickdelta); }));
  
     svg.append("g")
         .attr("class", "axis yaxis")
         .attr("transform", "translate(" + width + ",0)")
            .call(d3.axisRight(y).ticks(Math.floor(height/15)));
 
      var barwidth    = x.bandwidth();
      var fillwidth   = (Math.floor(barwidth*0.9)/2)*2+1;
      var bardelta    = Math.round((barwidth-fillwidth)/2);
  
      var mbar = svg.selectAll("."+mname+"bar")
          .data([data])
        .enter().append("g")
            .attr("class", mname+"bar")
        .selectAll("rect")
          .data(function(d) { return d; })
            .enter();
        mbar.append("rect")
          .attr("class", mname+"fill")
          .attr("x", function(d) { return x(d.TIMESTAMP) + bardelta; })
          .attr("y", function(d) { return y(d[MValue]); })
          .attr("class", function(d, i) { return mname+i; })
          .attr("height", function(d) { return y(0) - y(d[MValue]); })
          .attr("width", fillwidth);
        var mbar = mbar.append("g")
          .attr("id", function(d, i) {return "accessvolume-" + d3.timeFormat("%d-%m-%Y")(d.TIMESTAMP);})
          .attr("display", "none")
          .attr("class", "tick")
          .attr("transform",  function(d, i){return "translate(" + width + "," + y(d.ACCESS) + ")";});
        mbar.append("line")
            .attr("stroke", "#fff")
            .attr("x2", -width);
        mbar.append("rect")
            .attr("fill", "#292f3b")
            .attr("x", fillwidth)
            .attr("y", "-6px")
            .attr("height", "12px")
            .attr("width", "7ex");
        mbar.append("text")
            .attr("fill", "currentColor")
            .attr("x", fillwidth)
            .attr("dy", ".32em")
            .text( function(d, i){ return Math.round(d.ACCESS * 100) / 100;});
    });
  } // barrender
  barrender.mname = function(value) {
          	if (!arguments.length) return mname;
          	mname = value;
          	return barrender;
      	};

  barrender.margin = function(value) {
          	if (!arguments.length) return margin.top;
          	margin.top = value;
          	return barrender;
      	};

  barrender.MValue = function(value) {
          	if (!arguments.length) return MValue;
          	MValue = value;
          	return barrender;
      	};

return barrender;
} // barchart

function csaccessbar(data, display) {
    var width = 620, height = 60;
    var y = d3.scaleLinear()
          .rangeRound([height, 0]);
    y.domain([0, d3.max(data, function(d) { return d.ACCESS; })]).nice();
    d3.select("#accessvolume-" + d3.timeFormat("%d-%m-%Y")(data.TIMESTAMP)).attr("display", display);
}
