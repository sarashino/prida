function csheader() {
var MAVG = 'MAVG25';
function cshrender(selection) {
  selection.each(function(data) {
    var interval   = TIntervals[TPeriod];
    var format     = (interval=="month")?d3.timeFormat("%b %Y"):d3.timeFormat("%b %d %Y");
    var dateprefix = (interval=="month")?"Month of ":(interval=="week")?"Week of ":"";
    d3.select("#infodate").text(dateprefix + format(data.TIMESTAMP));
    d3.select("#infoopen").text("O " + (Math.round(data.OPEN * 100) / 100));
    d3.select("#infohigh").text("H " + (Math.round(data.HIGH * 100) / 100));
    d3.select("#infolow").text("L " + (Math.round(data.LOW * 100) / 100));
    d3.select("#infoclose").text("C " + (Math.round(data.CLOSE * 100) / 100));
    d3.select("#infomavg").text("MA " + (Math.round(data[MAVG] * 100) / 100));

  });
} // cshrender
cshrender.MAVG = function(value) {
if (!arguments.length) return MAVG;
MAVG = value;
return cshrender;
};

return cshrender;
} // csheader
