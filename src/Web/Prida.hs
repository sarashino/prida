-- | Hablog is a simple blog which fetches contents from disc.
--   It features posts, tags, multiple authors, code highlighting and more.
--   For more information, consult the README

module Web.Prida
  ( -- * Modules
    module Web.Prida.Run
  , module Web.Prida.Types
  , module Web.Prida.Config
  , partition
  )
  where

import Web.Prida.Run
import Web.Prida.Types
import Web.Prida.Config
import Web.Prida.Utils(partition)
