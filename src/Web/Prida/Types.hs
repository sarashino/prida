 -- | Types to define Hablog over ScottyT

{-# LANGUAGE OverloadedStrings #-}

module Web.Prida.Types where

import Data.Text.Lazy (Text)
import Web.Scotty.Trans (ScottyT, ActionT)
import Control.Monad.Trans.Class (lift)
import Control.Monad.Trans.Reader (ReaderT, ask)

import Web.Prida.Config (Config)

type Prida = ScottyT Text (ReaderT Config IO)
type PridaAction = ActionT Text (ReaderT Config IO)

data PridaAnalyses = ResTime
                   | DeviceKindRat
                   | AgentRat
                   | PathRat
                   | OSRat
                   | ReferedFromRat

data Anchor = Time Text | Tag Text | Author Text | Raw

getCfg :: PridaAction Config
getCfg = lift ask
