-- | Running Hablog

{-# LANGUAGE OverloadedStrings #-}

module Web.Prida.Run where


import Control.Monad (void)
import Control.Concurrent (forkIO)
import           Control.Monad.IO.Class (liftIO)
import           Web.Scotty (scotty)
import           Web.Scotty.Trans
import           Web.Scotty.TLS (scottyTTLS)
import           Control.Monad.Trans.Reader (runReaderT)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import qualified Data.Text.Lazy as TL
import qualified Text.Blaze.Html.Renderer.Text as HR
import qualified Network.Mime as Mime (defaultMimeLookup)
import Network.Wai.Handler.Warp (setPort, defaultSettings)
import Network.URI (URI, parseURI)
import Network.HTTP.Types (status404,status301)
import Control.Monad
import Data.Maybe

import Web.Prida.Types
import Web.Prida.Config
import Web.Prida.Present
import Web.Prida.Html (errorPage)
import Web.Prida.Post (eqY, eqYM, eqDate)
import Web.Prida.Logger (initLogger)

import Data.Default.Class (def)

-- | Run Hablog
run :: Config -> IO ()
run cfg = do
  logger <- initLogger $ logDB cfg
  let opts = def { settings =
                     setPort (blogPort cfg) $
                     defaultSettings}
  case blogTLSConfig cfg of
    (Just tlsCfg) -> do
        void $ forkIO $
            scotty 80 $ do
                get (regex ".*") $ do
                    path <- param "0"
                    status status301
                    setHeader "Location" $ (blogDomain cfg') <> path
        scottyTTLS (blogPort cfg) (blogKey tlsCfg) (blogCert tlsCfg) (`runReaderT` cfg') $ do
          middleware logger
          (router $! domain)
    Nothing ->
      scottyOptsT opts (`runReaderT` cfg') $ do
        middleware logger
        (router $! domain)
  where
    cfg' = case (blogTLSConfig cfg) of
             (Just _) -> cfg
                  { blogDomain = "https://" <> blogDomain cfg <> portStr }
             Nothing -> cfg
                  { blogDomain = "http://" <> blogDomain cfg <> portStr }
    portStr = if (blogPort cfg) == defaultPort then "" else ":" <> TL.pack (show (blogPort cfg))
    domain = parseURI (TL.unpack $ blogDomain cfg')


-- | Hablog's router
router :: Maybe URI -> Prida ()
router domain = do
  get "/" $ presentPage "home"

  get "/favicon.ico" $ file "static/img/favicon.ico"

  get (regex "^/apple-touch-icon.*\\.png$") $ file "static/img/apple-touch-icon.png"

  get "/blog" presentBlog

  get "/cv" $ do
        setHeader "content-type" $ TL.fromStrict (T.decodeUtf8 "text/html")
        file "static/html/cv.html"

  get "/statistics" $ do
        setHeader "content-type" $ TL.fromStrict (T.decodeUtf8 "text/html")
        file "static/html/statistics.html"

  -- get "/statistics/device-family-ratio.csv" $ analyseLog DeviceKindRat
  -- get "/statistics/os-ratio.csv" $ analyseLog OSRat
  -- get "/statistics/user-agent-ratio.csv" $ analyseLog AgentRat
  -- get "/statistics/asked-path-ratio.csv" $ analyseLog PathRat
  -- get "/statistics/response-time.csv" $ analyseLog ResTime
  get "/statistics/log.csv" $ do
        setHeader "content-type" $ "text/csv"
        file "static/test-data/log.csv"

  get (regex "/static/(.*)") $ do
    path <- fmap (drop 1 . T.unpack) (param "0")
    if hasdots path then
      fail "no dots in path allowed."
      else do
        let mime = Mime.defaultMimeLookup (T.pack path)
        setHeader "content-type" $ TL.fromStrict (T.decodeUtf8 mime)
        file path

  route domain

  get "/:page" $ do
    page <- param "page"
    presentPage (TL.toLower page)

  get (regex "/*") $ do
      cfg <- getCfg
      pages <- liftIO getAllPages
      status status404
      raise "404: Not Found" `rescue`
          (\msg -> html . HR.renderHtml $ errorPage cfg msg (TL.unpack msg) pages)

  where
    hasdots [] = False
    hasdots ('.':'.':_) = True
    hasdots (_:rest) = hasdots rest


route :: Maybe URI -> Prida ()
route domain = do
  when (isJust domain)
    $ get "/blog/rss" (presentRSS $ fromJust domain)

  get "/blog/post/:yyyy/:mm/:dd/:title" $ do
    (yyyy, mm, dd) <- getDate
    title <- param "title"
    presentPost (mconcat [yyyy,"/",mm,"/",dd, "/", title])

  get "/blog/post/:yyyy/:mm/:dd" $ do
    (yyyy, mm, dd) <- getDate
    showPostsWhere (eqDate (yyyy, mm, dd)) $ dd <> "/" <> mm <> "/" <> yyyy

  get "/blog/post/:yyyy/:mm" $ do
    yyyy <- param "yyyy"
    mm <- param "mm"
    showPostsWhere (eqYM (yyyy, mm)) $ mm <> "/" <> yyyy

  get "/blog/post/:yyyy" $ do
    yyyy <- param "yyyy"
    showPostsWhere (eqY yyyy) $ yyyy

  get "/blog/tags"
    presentTags

  get "/blog/tags/:tag" $ do
    tag <- param "tag"
    presentTag tag

  get "/blog/authors"
    presentAuthors

  get "/blog/authors/:author" $ do
    author <- param "author"
    presentAuthor author

  -- redirects

  when (isJust domain)
    $ get "/rss" $ do
      redirect "/blog/rss"

  get "/post/:yyyy/:mm/:dd/:title" $ do
    (yyyy, mm, dd) <- getDate
    title <- param "title"
    redirect $ mconcat ["/blog/post/", yyyy, "/", mm, "/", dd, "/", title]

  get "/post/:yyyy/:mm/:dd" $ do
    (yyyy, mm, dd) <- getDate
    redirect $ mconcat ["/blog/post/", yyyy, "/", mm, "/", dd]

  get "/post/:yyyy/:mm" $ do
    yyyy <- param "yyyy"
    mm <- param "mm"
    redirect $ mconcat ["/blog/post/", yyyy, "/", mm]

  get "/post/:yyyy" $ do
    yyyy <- param "yyyy"
    redirect $ mconcat ["/blog/post/", yyyy]

  get "/tags" $ do
    redirect "/blog/tags"

  get "/tags/:tag" $ do
    tag <- param "tag"
    redirect $ mconcat ["/blog/tags/", tag]

  get "/authors" $ do
    redirect "/blog/authors"

  get "/authors/:author" $ do
    author <- param "author"
    redirect $ mconcat ["/blog/authors/", author]

  where
    getDate =  (,,)
           <$> param "yyyy"
           <*> param "mm"
           <*> param "dd"
