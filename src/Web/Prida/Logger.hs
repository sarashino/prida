-- | Logging Request detail

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Web.Prida.Logger where

import Data.List as L
import Data.Time (getCurrentTime, UTCTime, diffUTCTime, NominalDiffTime, diffTimeToPicoseconds)
import Data.Time.Clock.POSIX (utcTimeToPOSIXSeconds)
import qualified Data.ByteString as BS
import Data.Maybe ()
import Control.Concurrent (forkIO)
import Control.Monad (void)
import Network.Wai
import Network.Wai.Internal (Response(..))
import qualified Network.HTTP.Types as H
import Network.Socket (SockAddr (SockAddrInet))
import Database.PostgreSQL.Simple
import Database.PostgreSQL.Simple.ToRow (toRow)
import Database.PostgreSQL.Simple.ToField (ToField(..))
import Data.Pool (Pool, withResource, createPool)
import Web.UAParser

-- logging to log.db
data LogField = LogField
  { getDatetime :: UTCTime
  , getRemoteHost :: SockAddr
  , getRequestMethod :: H.Method
  , getStatusCode :: Maybe Int
  , getPath :: BS.ByteString
  , getQuery :: BS.ByteString
  , getRequestHeaderHost :: Maybe BS.ByteString
  , getRequestHeaderReferer :: Maybe BS.ByteString
  , getRequestHeaderUserAgent :: Maybe BS.ByteString
  , getResponseTime :: NominalDiffTime } deriving (Show)

-- instance FromRow LogField where
    -- fromRow = LogField <$> field <*> field

instance ToRow LogField where
  toRow lf =
          [ toField $ nominalDiffTimeToMilliseconds . utcTimeToPOSIXSeconds . getDatetime $ lf
          , toField ( fromIntegral . (\(SockAddrInet _ y) -> y) . getRemoteHost $ lf :: Int )
          , toField ( fromIntegral . (\(SockAddrInet x _) -> x) . getRemoteHost $ lf :: Int )
          , toField $ getRequestMethod $ lf
          , toField $ getStatusCode $ lf
          , toField $ getPath $ lf
          , toField $ getQuery $ lf
          , toField $ getRequestHeaderHost lf
          , toField $ getRequestHeaderReferer lf
          , toField $ getRequestHeaderUserAgent lf >>= parseUA >>= \x -> (Just $ uarFamily x)
          , toField $ getRequestHeaderUserAgent lf >>= parseUA >>= \x -> (Just $ uarVersion x)
          , toField $ getRequestHeaderUserAgent lf >>= parseOS >>= \x -> (Just $ osrFamily x)
          , toField $ getRequestHeaderUserAgent lf >>= parseOS >>= \x -> (Just $ osrVersion x)
          , toField $ getRequestHeaderUserAgent lf >>= parseDev >>= \x -> (Just $ drFamily x)
          , toField $ getRequestHeaderUserAgent lf >>= parseDev >>= drBrand
          , toField $ getRequestHeaderUserAgent lf >>= parseDev >>= drModel
          , toField . logInt . nominalDiffTimeToNanoseconds . getResponseTime $ lf
          ]
        where
            logInt :: Integer -> Double
            logInt = log . fromIntegral

initLogger :: ConnectInfo -> IO (Middleware)
initLogger ci = do
    conn <- connect ci
    [Only (exists :: Int)] <- query_ conn "SELECT COUNT(1) FROM pg_database WHERE datname = 'prida';"
    _ <- if exists == 1
       then pure 0
       else execute_ conn "CREATE DATABASE prida;"
    conn <- connect $ ci { connectDatabase = "prida" }
    [Only (exists :: Int)] <- query_ conn "SELECT COUNT(1) FROM pg_user WHERE usename = 'prida';"
    _ <- if exists == 1
       then pure 0
       else execute_ conn "CREATE USER prida;"
    [Only (exists :: Int)] <- query_ conn "SELECT COUNT(1) FROM pg_user WHERE usename = 'statistics';"
    _ <- if exists == 1
       then pure 0
       else execute_ conn "CREATE USER statistics;"
    _ <- execute_ conn "CREATE SCHEMA IF NOT EXISTS prida;"
    _ <- execute_ conn "GRANT CONNECT \
            \ ON DATABASE prida \
            \ TO prida, statistics;"
    _ <- execute_ conn "GRANT USAGE \
            \ ON SCHEMA prida \
            \ TO prida, statistics;"
    _ <- execute_ conn "CREATE TABLE IF NOT EXISTS prida.log \
                      \ ( surrkey BIGSERIAL PRIMARY KEY \
                      \ , datetime BIGSERIAL NOT NULL \
                      \ , remote_host BIGSERIAL NOT NULL\
                      \ , remote_host_port SERIAL NOT NULL\
                      \ , request_method VARCHAR(10) NOT NULL\
                      \ , statuscode SERIAL \
                      \ , path VARCHAR(255) NOT NULL\
                      \ , query VARCHAR(255) \
                      \ , request_header_host VARCHAR(255) \
                      \ , referer VARCHAR(255) \
                      \ , user_agent VARCHAR(255 ) \
                      \ , user_agent_version VARCHAR(255 ) \
                      \ , user_agent_os VARCHAR(255) \
                      \ , user_agent_os_version VARCHAR(255) \
                      \ , user_agent_device VARCHAR(255) \
                      \ , user_agent_device_brand VARCHAR(255) \
                      \ , user_agent_device_model VARCHAR(255) \
                      \ , log_pico_response_time DOUBLE PRECISION NOT NULL\
                      \   );"
    -- execute_ conn "CREATE TABLE IF NOT EXISTS prida.statistics \
                      -- \ ( date DATE PRIMARY KEY\
                      -- \ , path  VARCHAR(32) NOT NULL \
                      -- \ , access_amount  SERIAL NOT NULL \
                      -- \ , agent_fst_name  VARCHAR(126) NOT NULL \
                      -- \ , agent_fst_percent  INTEGER NOT NULL \
                      -- \ , agent_snd_name  VARCHAR(126) \
                      -- \ , agent_snd_percent  INTEGER \
                      -- \ , agent_thd_name  VARCHAR(126) \
                      -- \ , agent_thd_percent  INTEGER \
                      -- \ , agent_frt_name  VARCHAR(126) \
                      -- \ , agent_frt_percent  INTEGER \
                      -- \ , agent_fif_name  VARCHAR(126) \
                      -- \ , agent_fif_percent  INTEGER \
                      -- \ , agent_other_name  VARCHAR(126) \
                      -- \ , agent_other_percent  INTEGER \
                      -- \ , os_fst_name  VARCHAR(126) NOT NULL \
                      -- \ , os_fst_percent  INTEGER NOT NULL \
                      -- \ , os_snd_name  VARCHAR(126) \
                      -- \ , os_snd_percent  INTEGER \
                      -- \ , os_thd_name  VARCHAR(126) \
                      -- \ , os_thd_percent  INTEGER \
                      -- \ , os_frt_name  VARCHAR(126) \
                      -- \ , os_frt_percent  INTEGER \
                      -- \ , os_fif_name  VARCHAR(126) \
                      -- \ , os_fif_percent  INTEGER \
                      -- \ , os_other_name  VARCHAR(126) \
                      -- \ , os_other_percent  INTEGER \
                      -- \ , referred_by_fst_name  VARCHAR(126) NOT NULL \
                      -- \ , referred_by_fst_percent  INTEGER NOT NULL \
                      -- \ , referred_by_snd_name  VARCHAR(126) \
                      -- \ , referred_by_snd_percent  INTEGER \
                      -- \ , referred_by_thd_name  VARCHAR(126) \
                      -- \ , referred_by_thd_percent  INTEGER \
                      -- \ , referred_by_frt_name  VARCHAR(126) \
                      -- \ , referred_by_frt_percent  INTEGER \
                      -- \ , referred_by_fif_name  VARCHAR(126) \
                      -- \ , referred_by_fif_percent  INTEGER \
                      -- \ , referred_by_other_name  VARCHAR(126) \
                      -- \ , referred_by_other_percent  INTEGER \
                      -- \ )"
    _ <- execute_ conn "GRANT SELECT, INSERT, UPDATE, DELETE \
                      \ ON ALL TABLES IN SCHEMA prida \
                      \ TO prida;"
    _ <- execute_ conn "GRANT SELECT, DELETE \
                      \ ON TABLE prida.log \
                      \ TO statistics;"
    _ <- execute_ conn "GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA prida TO prida;"
    -- execute_ conn "GRANT SELECT, INSERT, UPDATE, DELETE \
                      -- \ ON TABLE prida.statistics \
                      -- \ TO statistics;"
    close conn
    db <- createPool (connect $ ci {connectUser = "prida", connectDatabase = "prida"})
                           close
                           2 -- stripes
                           20 -- unused connections are kept open for a minute
                           50 -- max connection
    pure $ logger db
logger :: Pool Connection -> Middleware
logger db app req sendResponse = do
    t0 <- getCurrentTime
    app req $ \rsp -> do
        let isRaw =
                case rsp of
                  ResponseRaw{} -> True
                  _ -> False
        t1 <- getCurrentTime
        let logRow = LogField
              { getDatetime = t0
              , getRemoteHost = remoteHost req
              , getRequestMethod = requestMethod req
              , getStatusCode = if isRaw then Nothing else Just $ H.statusCode . responseStatus $ rsp
              , getPath = rawPathInfo req
              , getQuery = rawQueryString req
              , getRequestHeaderHost = requestHeaderHost req
              , getRequestHeaderReferer = requestHeaderReferer req
              , getRequestHeaderUserAgent = requestHeaderUserAgent req
              , getResponseTime = diffUTCTime t1 t0
              }
        void $ forkIO $ do
            insertLog db logRow

        sendResponse rsp
    where
        insertLog :: Pool Connection -> LogField -> IO ()
        insertLog db' lf = do
                        withResource db'
                               $ \conn -> execute conn "INSERT INTO prida.log VALUES (DEFAULT,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" lf
                        pure ()

nominalDiffTimeToPicoseconds :: NominalDiffTime -> Integer
nominalDiffTimeToPicoseconds = diffTimeToPicoseconds . realToFrac
nominalDiffTimeToNanoseconds :: NominalDiffTime -> Integer
nominalDiffTimeToNanoseconds a = fromIntegral $ (nominalDiffTimeToPicoseconds $ a) `div` (10^(3 :: Int))
nominalDiffTimeToMilliseconds :: NominalDiffTime -> Integer
nominalDiffTimeToMilliseconds a = fromIntegral $ (nominalDiffTimeToPicoseconds $ a) `div` (10^(9 :: Int))
