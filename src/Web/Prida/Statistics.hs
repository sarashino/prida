-- | Logging Request detail

{-# LANGUAGE OverloadedStrings #-}

module Web.Prida.Statistics where
    -- logger
-- 1. generate statistic data from log.db and save it into statistics.db
-- 2. display the information as a diagram
                       -- " ( surrkey INTEGER PRIMARY KEY \
                       -- \ , datetime INTEGER NOT NULL \
                       -- \ , remote_host INTEGER NOT NULL\
                       -- \ , remote_host_port INTEGER NOT NULL\
                       -- \ , request_method TEXT NOT NULL\
                       -- \ , statuscode INTEGER \
                       -- \ , path TEXT NOT NULL\
                       -- \ , query TEXT \
                       -- \ , request_header_host TEXT \
                       -- \ , referer TEXT \
                       -- \ , user_agent TEXT \
                       -- \ , user_agent_version TEXT \
                       -- \ , user_agent_os TEXT \
                       -- \ , user_agent_os_version TEXT \
                       -- \ , user_agent_device TEXT \
                       -- \ , user_agent_device_brand TEXT \
                       -- \ , user_agent_device_model TEXT \
                       -- \ , response_time INTEGER NOT NULL\
                       -- \   )"
                       -- /statistics/response-time
                       --   TIMESTAMP, OPEN, CLOSE, LOW, HIGH, MEAN, MAVG5, MAVG10, MAVG25, MAVG50, MAVG75
                       -- /statistics/device-family-ratio
                       -- /statistics/os-ratio
                       -- /statistics/user-agent-ratio
                       -- /statistics/asked-path-ratio
