{-# LANGUAGE OverloadedStrings #-}

module Web.Prida.Utils where

import Control.Arrow ((&&&))
import qualified System.Directory as DIR
import qualified System.FilePath.Posix as FP
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Encoding as TL
import qualified Data.ByteString.Lazy as BSL
import qualified Data.ByteString as BS
import qualified Data.ByteString.Conversion as BSC
import Data.Digest.XXHash.FFI (xxh64)
import qualified Data.Text as T
import qualified Data.Text.Encoding as T
import           Data.Maybe (Maybe(..))
import Data.Either.Combinators (rightToMaybe)
import           System.IO.Error (catchIOError)
import Codec.Winery
import qualified Data.Map as M
import qualified CMarkGFM as GFM
import qualified Text.Blaze.Html5 as H
import qualified Text.Blaze.Internal as BI

hd :: [a] -> Maybe a
hd [] = Nothing
hd (x:_) = Just x

at :: [a] -> Int -> Maybe a
at [] _ = Nothing
at (x:xs) n
  | n > 0 = xs `at` (n-1)
  | n == 0 = Just x
  | otherwise = reverse xs `at` (-n)

headerBreaker :: TL.Text
headerBreaker = "---"

takeJust :: [Maybe a] -> Maybe a
takeJust [] = Nothing
takeJust (Just x:_) = Just x
takeJust (Nothing:xs) = takeJust xs

convert :: Char -> [String] -> String
convert c str = concatMap (++[c]) (init str) ++ last str

splitBy :: Char -> String -> [String]
splitBy c txt = map reverse $ go [] txt
  where go xs [] = [xs]
        go xs (y:ys)
          | y == c    = xs : go [] ys
          | otherwise = go (y:xs) ys

removeWhitespaces :: String -> String
removeWhitespaces = unwords . words

infixl 0 |>
(|>) :: a -> (a -> b) -> b
x |> f = f x

partition :: Char -> TL.Text -> (TL.Text, TL.Text)
partition c = TL.takeWhile (/=c) &&& (TL.unwords . TL.words . TL.dropWhile (==c) . TL.dropWhile (/=c))


getHeader :: TL.Text -> M.Map TL.Text TL.Text
getHeader = M.fromList . filter ((/=)"" . snd) . map (partition ':') . takeWhile (not . TL.isPrefixOf headerBreaker) . TL.lines

getContent :: TL.Text -> TL.Text
getContent = TL.unlines . dropWhile (TL.isPrefixOf headerBreaker) . dropWhile (not . TL.isPrefixOf headerBreaker) . TL.lines

cachedConstructor :: Serialise a => (TL.Text -> Maybe a) -> String -> TL.Text -> IO (Maybe a)
cachedConstructor constructor path rawBody = do
  cachedCont <- getCachedCont (".cache/" ++ path) . TL.encodeUtf8 $ rawBody
  case cachedCont of
      Just c -> do
          pure c
      Nothing -> do
          let content = constructor rawBody
          case hash . TL.encodeUtf8 $ rawBody of
              Just hashcode -> do
                      DIR.createDirectoryIfMissing True $ ".cache/" ++ FP.takeDirectory path
                      BS.writeFile (".cache/" ++ path ++ ".xxhash") $ T.encodeUtf8 . TL.toStrict $ hashcode
                      BS.writeFile (".cache/" ++ path ++ ".winery") $ serialise content
              _ -> do
                  putStrLn "cache failed: encodefailed"
                  pure ()
          pure content

getCachedCont :: Serialise a => String -> BSL.ByteString -> IO (Maybe a)
getCachedCont path cont = do
    hashcode <- rightToMaybe <$> (TL.decodeUtf8' <$> (BSL.readFile $ path <> ".xxhash")) `catchIOError` const (pure $ Left undefined)
    if hashcode == hash cont
        then do
            raw <- (BS.readFile $ path <> ".winery")
            pure . rightToMaybe $ deserialise raw
        else pure Nothing
hash :: BSL.ByteString -> Maybe TL.Text
hash = Just . TL.decodeUtf8 . BSC.toByteString . flip xxh64 0 . BSL.toStrict

createBody :: Bool -> Maybe TL.Text -> TL.Text -> TL.Text
createBody = ((TL.fromStrict .) .) . createBodyRaw

createBodyHtml :: T.Text -> H.Html
createBodyHtml t =
    BI.Content (BI.PreEscaped $ BI.Text $ t) ()
createBodyRaw :: Bool -> Maybe TL.Text -> TL.Text -> T.Text
createBodyRaw _ articleType t =
                   ((postProcess articleType)
                 . GFM.commonmarkToHtml
                   [ GFM.optSmart
                   , GFM.optUnsafe ]
                   [ GFM.extStrikethrough
                   , GFM.extTable
                   , GFM.extAutolink
                   , GFM.extTagfilter
                   ]
                 . TL.toStrict $ t)
    where
        postProcess :: Maybe TL.Text -> T.Text -> T.Text --for first h3 tag
        -- postProcess (Just "article-2col") t
                        -- =  "<div class=\"markdown-body content-2col\">\n"
                        -- <> (T.unlines . fst . splitAt (breakPoint t) $ T.lines t)
                        -- <> "<div class=\"col2-content\">"
                        -- <>  (postProcess' . snd . splitAt (breakPoint t) . T.lines . breakToSpaceInP $ t)
                        -- <> "</div>\n</div>"
        postProcess _ txt = "<div class=\"markdown-body content-1col\">"
                        <> (T.unlines . map (T.replace "<p>note:" "<p class=\"notes\">※") $ T.lines txt)
                        <> "</div>" --and divide into <div class="markdown-body content-2col"> cal2-left,col2-right

