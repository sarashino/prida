{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE DeriveGeneric #-}

module Web.Prida.Post where

import           Data.Monoid ((<>))
import qualified Data.Text.Lazy as TL
import qualified Data.Map as M
import qualified Text.RSS as RSS
import           Data.Time (fromGregorian, Day, UTCTime(..), secondsToDiffTime)
import           Network.URI (parseURI)
import GHC.Generics (Generic)
import Codec.Winery (Serialise(..), WineryVariant(..), WineryRecord(..))

import Web.Prida.Utils

data Post
  = Post
  { date  :: (TL.Text, TL.Text, TL.Text)
  , editDate  :: Maybe (TL.Text, TL.Text, TL.Text)
  , route :: TL.Text
  , title :: TL.Text
  , lang :: Lang
  , authors :: [TL.Text]
  , tags    :: [TL.Text]
  , leadingSummary :: TL.Text
  , summary :: TL.Text
  , content :: TL.Text
  } deriving (Generic)
  deriving Serialise via WineryRecord Post

data Lang = EN | JP
  deriving (Generic)
  deriving Serialise via WineryVariant Lang
instance Show Lang where
    show EN = "en"
    show JP = "jp"

year, month, day :: Post -> TL.Text
year  p = case date p of { (y, _, _) -> y; }
month p = case date p of { (_, m, _) -> m; }
day   p = case date p of { (_, _, d) -> d; }
editYear, editMonth, editDay :: Post -> Maybe TL.Text
editYear  p = case editDate p of {
                            Just (y, _, _) -> Just y;
                            Nothing -> Nothing; }
editMonth  p = case editDate p of {
                            Just (_, m, _) -> Just m;
                            Nothing -> Nothing; }
editDay  p = case editDate p of {
                            Just (_, _, d) -> Just d;
                            Nothing -> Nothing; }

toDay :: Post -> Maybe Day
toDay post =
  case (reads $ TL.unpack $ year post, reads $ TL.unpack $ month post, reads $ TL.unpack $ day post) of
    ([(y,[])], [(m,[])], [(d,[])]) -> pure (fromGregorian y m d)
    _ -> Nothing

toPost :: String -> TL.Text -> IO (Maybe Post)
toPost = cachedConstructor toPost'
toPost' :: TL.Text -> Maybe Post
toPost' fileContent =
  Post <$> ((,,) <$> yyyy <*> mm <*> dd)
       <*> Just ((,,) <$> edityyyy <*> editmm <*> editdd)
       <*> M.lookup "route" header
       <*> M.lookup "title" header
       <*> Just ((\l -> if l == Just "ja" then JP else EN) $ M.lookup "lang" header)
       <*> (map (TL.unwords . TL.words) . TL.split (==',') <$> M.lookup "authors" header)
       <*> (map (TL.toLower . TL.unwords . TL.words) . TL.split (==',') <$> M.lookup "tags" header)
       <*> M.lookup "leading-summary" header
       <*> M.lookup "summary" header
       <*> pure (createBody ((M.lookup "lang" header) == (Just "en")) (M.lookup "type" header) $ getContent fileContent)
    where
        header = getHeader fileContent
        dt mkey  = TL.split (=='-') <$> M.lookup mkey header
        yyyy   = (dt "date") >>= (`at` 0)
        mm     = (dt "date") >>= (`at` 1)
        dd     = (dt "date") >>= (`at` 2)
        edityyyy   = (dt "edited") >>= (`at` 0)
        editmm     = (dt "edited") >>= (`at` 1)
        editdd     = (dt "edited") >>= (`at` 2)

getPath :: Post -> TL.Text
getPath post =
  TL.concat ["post/", year post, "/", month post, "/", day post, "/", route post]

getDate :: Post -> TL.Text
getDate post =
  TL.concat [day post, "/", month post, "/", year post]

eqY, eqM, eqD :: TL.Text -> Post -> Bool
eqY y p = y == year  p
eqM m p = m == month p
eqD d p = d == day   p

eqYM :: (TL.Text, TL.Text) -> Post -> Bool
eqYM (y,m) p = eqY y p && eqM m p

eqDate :: (TL.Text, TL.Text, TL.Text) -> Post -> Bool
eqDate dt p = dt == date p

instance Show Post where
  show post =
    TL.unpack $ TL.concat ["post/", year post, "/", month post, "/", day post, "/", route post]

instance Eq Post where
  (==) p1 p2 = route p1 == route p2


instance Ord Post where
  compare p1 p2
    | year p1 < year p2 = LT
    | year p1 == year p2 && month p1 < month p2 = LT
    | year p1 == year p2 && month p1 == month p2 && day p1 < day p2 = LT
    | year p1 == year p2 && month p1 == month p2 && day p1 == day p2 = EQ
    | otherwise = GT


toRSS :: TL.Text -> Post -> RSS.Item
toRSS domain post =
  [ RSS.Title (TL.unpack $ title post)
  ] ++ map (RSS.Author . TL.unpack) (authors post)
    ++ map (RSS.Category Nothing . TL.unpack) (tags post)
    ++ [ RSS.PubDate $ UTCTime d (secondsToDiffTime 0)
       | Just d <- [toDay post]
       ]
    ++ [ RSS.Link r
       | Just r <- (:[]) $ parseURI $
           TL.unpack (domain <> "/" <> getPath post)
       ]
    ++ [ RSS.Description
       . TL.unpack
       $ content post
       ]


