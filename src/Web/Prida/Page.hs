{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE DeriveGeneric #-}

module Web.Prida.Page where

import           Control.Arrow  ((&&&))
import qualified Data.Map as M
import qualified Data.Text.Lazy as T
import qualified Data.Text.Lazy.Read as TR
import GHC.Generics (Generic)
import Codec.Winery (Serialise(..), WineryVariant(..))

import Web.Prida.Utils

data Page
    = Page
    { getPageURL          :: FilePath
    , getPageName         :: T.Text
    , getPageDescription  :: T.Text
    , getPagePriority     :: Int
    , getPageContent      :: T.Text
    }
    | Links
    { getPageName         :: T.Text
    , getPagePriority     :: Int
    , getLinksList        :: [(T.Text, T.Text)]
    } deriving (Generic)
  deriving Serialise via WineryVariant Page


toPage :: String -> T.Text -> IO (Maybe Page)
toPage = cachedConstructor toPage'
toPage' :: T.Text -> Maybe Page
toPage' fileContent =
  case (M.lookup "type" header) of
    (Just "links") ->
      Links <$> M.lookup "title" header
            <*> maybeDecimal (M.lookup "priority" header)
            <*> (Just $ filter ((/=)"" . snd) . map (partition ':') $ T.lines content)
    (Just "html") ->
      Page <$> fmap T.unpack (M.lookup "route" header)
           <*> M.lookup "title" header
           <*> M.lookup "description" header
           <*> maybeDecimal (M.lookup "priority" header)
           <*> pure (getContent fileContent)
    _ ->
      Page <$> fmap T.unpack (M.lookup "route" header)
           <*> M.lookup "title" header
           <*> M.lookup "description" header
           <*> maybeDecimal (M.lookup "priority" header)
           <*> pure (createBody ((M.lookup "lang" header) == (Just "en")) (Just "page") $ getContent fileContent)
    where (header, content) = (getHeader &&& getContent) fileContent
          maybeDecimal :: Maybe T.Text -> Maybe Int
          maybeDecimal (Just priority) = _maybeDecimal . TR.decimal $ priority
          maybeDecimal Nothing = Nothing
          _maybeDecimal :: Either String (Int, T.Text) -> Maybe Int
          _maybeDecimal (Right (a, _)) = Just a
          _maybeDecimal (Left _) = Nothing

instance Show Page where
  show = getPageURL

instance Eq Page where
  (==) p1 p2 = getPageURL p1 == getPageURL p2

instance Ord Page where
  compare p1 p2
    | getPagePriority p1 < getPagePriority p2 = LT
    | otherwise = GT

