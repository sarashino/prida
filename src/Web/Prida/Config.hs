-- | Configuration for Hablog

{-# LANGUAGE OverloadedStrings #-}

module Web.Prida.Config where

import Data.Text.Lazy (Text)
import Database.PostgreSQL.Simple (ConnectInfo, defaultConnectInfo)

-- | Data type to set the theme for your Hablog blog
data Theme = Theme
  { bgTheme   :: FilePath -- ^ General theme for hablog. a file path for a css file
  , codeTheme :: FilePath -- ^ Theme for code. a file path for a highlight.js css file
  }
  deriving (Show, Read)

-- | Configuration for Hablog
data Config = Config
  { blogTitle  :: Text
  , blogTagLine  :: Text
  , blogTheme  :: Theme
  , blogDomain :: Text
  , blogIP :: Text
  , blogPort :: Int
  , blogTLSConfig :: Maybe TLSConfig
  , logDB :: ConnectInfo
  , blogTwitter :: Text
  }
  deriving (Show, Read)

-- | Requires the needed values for runTLS
data TLSConfig = TLSConfig
  { blogKey     :: FilePath
  , blogCert    :: FilePath
  }
  deriving (Show, Read)

-- | A default configuration
defaultConfig :: Config
defaultConfig = Config
  { blogTitle = defaultTitle
  , blogTagLine = defaultTagLine
  , blogTheme = snd defaultTheme
  , blogDomain = defaultDomain
  , blogIP = defaultIP
  , blogPort = defaultPort
  , blogTLSConfig = Nothing
  , logDB = defaultConnectInfo
  , blogTwitter = defaultTwitter
  }

-- | "Hablog"
defaultTitle :: Text
defaultTitle = "prida"

defaultTagLine :: Text
defaultTagLine = "fork of Hablog"

defaultDomain :: Text
defaultDomain = "localhost"

defaultIP :: Text
defaultIP = "127.0.0.1"
-- | The default HTTP port is 80
defaultPort :: Int
defaultPort = 80

-- | The default HTTPS port is 443
defaultTLSPort :: Int
defaultTLSPort = 443

-- | The default is the dark theme
defaultTheme :: (String, Theme)
defaultTheme = ("prida", pridaTheme)

defaultTwitter :: Text
defaultTwitter = "@null"

darkTheme :: Theme
darkTheme = Theme "/static/css/dark.css" "/static/highlight/styles/hybrid.css"

lightTheme :: Theme
lightTheme  = Theme "/static/css/light.css" "/static/highlight/styles/docco.css"

pridaTheme :: Theme
pridaTheme  = Theme "/static/css/prida.css" "/static/highlight/styles/docco.css"

themes :: [(String, Theme)]
themes =
  [("dark",  darkTheme)
  ,("light", lightTheme)
  ,("prida", pridaTheme)
  ]
