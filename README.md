Prida -- fork of Hablog
======

A simple blog platform with tags. Made with Haskell and Scotty.

Prida will read posts written in Markdown from the `_posts` folder.

License
=======

Prida is licensed under MIT license. This means the Haskell source files in the src directory.
Highlight.js related content is not a part of Hablog and is not licensed by it.


Installation
============

```sh
git clone https://gitlab.com/sarashino/prida
cd prida
stack build
```


How to write a new post?
========================

1. All posts must go under the `/_posts/` directory
1. All pages must go under the `/_pages/` directory
3. The content of the post/page must correspond to a specific structure

## A Post's Structure

```markdown
title: <the title of the post>
route: <route to the post>
authors: <the author of the post, seperated, by, commas>
date: yyyy-mm-dd
tags: <tags for the post, separated, by, commas>

---

<The rest of the post in Markdown format>
```

## A Page's Structure

```markdown
title: <the title of the page>
route: <route to the page>
priority: <priority in menu table>
type: <currently, only "links" is meaningful>
lang: <en | ja | both>
description: <description of the page>
---

<The rest of the page in Markdown format>
<for links type, you can enumerate urls like below.
"name: url
 name: url"
>

```

