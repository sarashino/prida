-- | A simple executable to run Hablog

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import qualified Data.Map as M
import qualified Data.Text.Lazy as T
import Text.Read (readMaybe)
import Data.Maybe (fromMaybe)
import Data.Text.Lazy.Encoding (decodeUtf8',decodeUtf8)
import Data.ByteString.Lazy (readFile)
import Prelude hiding (readFile)
import System.IO.Error (catchIOError)
import Database.PostgreSQL.Simple (defaultConnectInfo, ConnectInfo(..))
import Web.Prida
import Options.Applicative

main :: IO ()
main = do
  cfgPath <- customExecParser (prefs showHelpOnError) parserInfo
  cfg <- getCfgFromFile (filePath cfgPath) $ config
  run cfg

data Options = Options
    {  filePath     :: FilePath
    } deriving Show

configFileP :: Parser FilePath
configFileP = strOption $ mconcat
    [ short 'c', long "config"
    , help "config file"
    , metavar "FILE"
    , value "config.map"
    , showDefaultWith id
    ]
optionsP :: Parser Options
optionsP = (<*>) helper $
    Options <$> configFileP
parserInfo :: ParserInfo Options
parserInfo = info optionsP $ mconcat
    [ fullDesc
    , progDesc "prida blog system."
    , header "prida"
    , footer "prida"
    , progDesc "prida blog system"
    ]

getCfgFromFile :: FilePath -> (M.Map T.Text T.Text -> Config) -> IO Config
getCfgFromFile cfgPath conductor = do
    m <- decodeUtf8' <$> (readFile cfgPath `catchIOError` \_ -> return "config file not found")
    let cont = case m of
              Left _  -> decodeUtf8 ""
              Right x -> x
    pure $ conductor . M.fromList . filter ((/=)"" . snd) . map (partition ':') . T.lines $ cont

config :: M.Map T.Text T.Text -> Config
config cfg = Config
    (getOrDefault "title" (blogTitle defaultConfig) cfg)
    (getOrDefault "tagline" (blogTagLine defaultConfig) cfg)
    (getThemeOrDefault "theme" (blogTheme defaultConfig) cfg)
    (getOrDefault "domain-name" (blogDomain defaultConfig) cfg)
    (getOrDefault "ip" (blogIP defaultConfig) cfg)
    (getPortOrDefault "port" cfg)
    (toTLSConfig cfg)
    (getLogDB cfg)
    (getOrDefault "twitter" (blogTwitter defaultConfig) cfg)
  where
    getOrDefault :: T.Text -> T.Text -> M.Map T.Text T.Text -> T.Text
    getOrDefault h r c = case M.lookup h c of
                         Nothing -> r
                         (Just "") -> r
                         (Just a) -> a
    getThemeOrDefault :: T.Text -> Theme -> M.Map T.Text T.Text -> Theme
    getThemeOrDefault h r c = case M.lookup h c of
                         Nothing -> r
                         (Just "") -> r
                         (Just a) -> readTheme $ T.unpack a
    getPortOrDefault :: T.Text -> M.Map T.Text T.Text -> Int
    getPortOrDefault h c = let r = case (M.lookup "key-path" c, M.lookup "cert-path" c) of
                                    (Nothing, _) -> defaultPort
                                    (_, Nothing) -> defaultPort
                                    _ -> defaultTLSPort
                           in
                        case M.lookup h c of
                          Nothing -> r
                          (Just "") -> r
                          (Just a) -> (read (T.unpack a) :: Int)
    toTLSConfig c = TLSConfig <$> (lookupNotBlank "key-path" c)
                              <*> (lookupNotBlank "cert-path" c)
    lookupNotBlank :: T.Text -> M.Map T.Text T.Text -> Maybe String
    lookupNotBlank t c = case M.lookup t c of
                         Nothing -> Nothing
                         (Just "") -> Nothing
                         (Just r)-> Just (T.unpack r)

    getLogDB :: M.Map T.Text T.Text -> ConnectInfo
    getLogDB c = fromMaybe defaultConnectInfo
                       $ ConnectInfo <$> (lookupNotBlank "db-host" c)
                                     <*> (readMaybe =<< lookupNotBlank "db-port" c)
                                     <*> (lookupNotBlank "db-user" c)
                                     <*> (lookupNotBlank "db-password" c)
                                     <*> (lookupNotBlank "db-database" c)

readTheme :: String -> Theme
readTheme themeStr =
  case lookup themeStr themes of
    Nothing -> pridaTheme
    (Just a) -> a

