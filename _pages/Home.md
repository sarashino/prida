route: home
title: Home
priority: 1
type: md
lang: en
description: Sarashino's blog home

---
## Home

My name is Sarashino Misaki.
I'm a software developer in Tokyo/Japan.

This blog is inspired by [Gil Mizrahi](https://gilmi.me)'s [Hablog](https://github.com/soupi/hablog) and [Stephen Diehl's blog](http://www.stephendiehl.com).

The source code is hosted [here](https://gitlab.com/sarashino/prida) on gitlab.

Languages I've ever used until now 7/4/2020 is Common Lisp, Haskell, PureScript, Rust, Python3, C, Swift a little, Java SE 8+, and JavaScript ES6.

I like thinking about user centric design including UI/UX and software architecture.
Additionally, sustainability of UI/UX, software, and supply and demand chain is really count for me.


